#!/usr/bin/env python

import csv
import re


def process_chemical_composition_file(file):
    with open(file) as input_file, \
            open(CHEMICAL_COMPOSITIONS_FILE_PROCESSED, 'w') as output_file:
        reader = csv.reader(input_file)
        writer = csv.writer(output_file)
        header = next(reader)
        header.pop(0)
        writer.writerow(['id', 'limit_type', 'percentage', 'element'])
        for row in reader:
            writer.writerows(transpose_chemical_composition_row(header, row))


def transpose_chemical_composition_row(header, row):
    id = row.pop(0)
    rows = []
    for index, percentage in enumerate(row):
        if percentage:
            element, limit_type = header[index].split('_')
            percentage = percentage.replace(',', '.')
            rows.append([id, limit_type, percentage, element])
    return rows


def clean_materials_file(file):
    with open(file) as materials_file, \
            open(MATERIALS_CLEANED_FILE, 'w') as cleaned_file:
        reader = csv.reader(materials_file)
        writer = csv.writer(cleaned_file)
        header = next(reader)
        writer.writerow(header)
        material = [
            "Cast",
            "Centrifugal Cast",
            "Continuous Cast",
            "Ingot",
            "Permanent Mould Cast",
            "Forgings"
            "Pressure Die Cast",
            "Profile",
            "Rod",
            "Sand Cast",
            "Sheet",
            "Strip",
            "Tube",
            "Wire",
            "Wrought Alloy",
        ]
        for row in reader:
            row[4] = row[4].replace('ing', '')
            if row[4] in material:
                row.append(row[3])
                row[3] = clean_norm_string(row[3]) or 'PLEASEKILLME'
                writer.writerow(row)


def clean_norm_string(string):
    # EN 12451; 1999 > EN 12451 : 1999
    string = re.sub(r'(\d); (\d)', r'\1 : \2', string)
    for stuff in ['4505', '4508', '4533', '4554', '4651', '4713', '4870', '4871', '4873']:
        string = re.sub(r'(AMS \d.*?)(' + stuff + ')', r'\1AMS \2', string)
    string = string \
        .replace(')', ' ') \
        .replace(' ; ', ' : ') \
        .replace('"', ' ') \
        .replace('(', ' ') \
        .replace(']', ' ') \
        .replace(' . ', ' : ') \
        .replace('[', ' ') \
        .replace('{', ' ') \
        .replace('::', ':') \
        .replace('?', ' ') \
        .replace('}', ' ') \
        .replace('@', ' ') \
        .replace('früher', ',') \
        .replace('ßßßßß', '') \
        .replace('+', '') \
        .replace(';', ',') \
        .replace('und', ',') \
        .replace(',,', ',') \
        .replace('<', ':') \
        .replace('et alia', ' ') \
        .replace(' rod welding', ' ')
    string = re.sub(r'\s?/\s*', '/', string)
    string = re.sub(r'\s?:\s?', ' : ', string)
    string = re.sub(r'\s?-\s?', '-', string)
    string = re.sub(r'\s\s+', ' ', string)
    string = re.sub(r'(\d)\sM', r'\1M', string)
    string = re.sub(r'F (\d)', r'F\1', string)
    # DS 3001 1978
    string = re.sub(r'(\d) 1978', r'\1 : 1978', string)
    # BS 1432 : C 102 >> BS 1432
    string = re.sub(r' : C \d{3}', '', string)
    # AS 1566 : 110 >> AS 1566
    string = re.sub(r' : 1[^9][^7]', '', string)
    # UNI 5035 : 1962-09 >> UNI 5035 : 1962
    string = re.sub(r'( : \d{4})-\d{2}', r'\1', string)
    # UNI 7280-1 1974 >> UNI 7280-1 : 1974
    string = re.sub(r'(\d) (19\d{2})', r'\1 : \2', string)
    string = string.strip(' -,:xX') \
        .replace('DIN 17658 : 1873-06', 'DIN 17658 : 1973-06') \
        .replace('AS 2738.1', 'AS 2738-1') \
        .replace('UNI 2527-1UNI 2527-1', 'UNI 2527-1')
    return string


def extract_norms_from_materials_file(file):
    with open(file) as input_file, open(NORMS_FILE, 'w') as output_file:
        reader = csv.reader(input_file)
        writer = csv.writer(output_file)
        next(reader)
        writer.writerow(['norm', 'norm_processed', 'norm_sources'])
        # norms = {row[3]: [row[3], row[6]] for row in reader if row[3]}
        norms = {}
        for row in reader:
            if row[3] in norms:
                norms[row[3]][1].add(row[6])
            else:
                norms[row[3]] = [row[3], {row[6]}]
        norms_splitted = {}
        for norm, original_norm in norms.items():
            split_norms = {
                split_norm.strip(): original_norm for split_norm in norm.split(',')}
            for norm, source in split_norms.items():
                if norm in norms_splitted:
                    norms_splitted[norm][1].update(source[1])
                else:
                    norms_splitted[norm] = source
        for k, v in sorted(norms_splitted.items()):
            if k and k not in ['X', 'A', 'XX', 'c', 'm', 'rod', 'en 16625', 'Schweißzusatz'] and not any(substring in k for substring in ['berein', 'BEREIN', 'nicht', 'siehe', 'welding', 'brazing', 'Keine']):
                writer.writerow([k] + [v[0]] + list(v[1]))


def split_materials(file):
    with open(file) as materials_file, \
            open(MATERIAL_RAWS_FILE, 'w') as raws_file, \
            open(MATERIAL_FORMS_FILE, 'w') as forms_file:
        materials_reader = csv.reader(materials_file)
        raws_writer = csv.writer(raws_file)
        forms_writer = csv.writer(forms_file)
        header = next(materials_reader)
        forms_writer.writerow(header)
        raws_writer.writerow(header)
        matches = ["Ingot", "Cast", "Wrought"]
        for row in materials_reader:
            if any(match in row[4] for match in matches):
                raws_writer.writerow(row)
            else:
                forms_writer.writerow(row)


if __name__ == '__main__':
    CHEMICAL_COMPOSITIONS_FILE = 'chemical-compositions.csv'
    CHEMICAL_COMPOSITIONS_FILE_PROCESSED = 'chemical-compositions-processed.csv'
    MATERIALS_FILE = 'materials.csv'
    MATERIALS_CLEANED_FILE = 'materials-cleaned.csv'
    MATERIAL_RAWS_FILE = 'material-raws.csv'
    MATERIAL_FORMS_FILE = 'material-forms.csv'
    NORMS_FILE = 'norms.csv'

    process_chemical_composition_file(CHEMICAL_COMPOSITIONS_FILE)
    clean_materials_file(MATERIALS_FILE)
    extract_norms_from_materials_file(MATERIALS_CLEANED_FILE)
    split_materials(MATERIALS_CLEANED_FILE)
