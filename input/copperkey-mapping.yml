---
base: https://kupferdigital.gitlab.io/ontologies/copper-key#
prefixes:
  cd: https://kupferdigital.gitlab.io/ontologies/copper-key#
  qb: https://www.w3.org/TR/vocab-data-cube/
  rdfs: http://www.w3.org/2000/01/rdf-schema#
  grel: http://users.ugent.be/~bjdmeest/function/grel.ttl#
  owl: http://www.w3.org/2002/07/owl#
  om: http://www.ontology-of-units-of-measure.org/resource/om-2/
  time: http://www.w3.org/2006/time#
  xsd: http://www.w3.org/2001/XMLSchema#
  idlab-fn: http://example.com/idlab/function/
  qk: http://qudt.org/2.1/vocab/quantitykind/
  qudt: http://qudt.org/2.1/schema/qudt/
  sto: https://w3id.org/i40/sto#
  iof: https://purl.industrialontologies.org/ontology/core/Core/

sources:
  chemical-compositions: [chemical-compositions-processed.csv~csv]
  datasheets: [datasheets.csv~csv]
  material-forms: [material-forms.csv~csv]
  # Castings, Ingots, Wrought Alloys
  material-raws: [material-raws.csv~csv]
  norms: [norms.csv~csv]

mappings:
  # A material is not the same as it specification
  material-raws:
    sources: material-raws
    subjects:
      function: grel:array_join
      parameters:
        - [grel:p_array_a, cd:]
        - parameter: grel:p_array_a
          value:
            function: idlab-fn:random
            parameters: [] # This is needed atm, bug in the YARRRML parser
    predicateobjects:
      - [a, cd:CopperAlloy~iri]
      - [rdfs:label, $(materialtype) ($(symbol))]
      - [cd:materialSymbol, $(symbol)]
      - [cd:materialNumber, $(number)]
      - predicates: a
        objects:
          # E.g. 'Sand Casting' is not a valid URI,
          # so replace all spaces with nothing
          function: idlab-fn:concat
          parameters:
            - [idlab-fn:str, cd:]
            - parameter: idlab-fn:otherStr
              value:
                function: grel:string_replace
                parameters:
                  - [grel:valueParameter, $(materialtype)]
                  - [grel:p_string_find, " "]
                  - [grel:p_string_replace, ""]
      - predicates: rdfs:isDefinedBy
        objects:
          mapping: norms
          condition:
            function: grel:string_contains
            parameters:
              - [grel:valueParameter, "$(norm)", s]
              - [grel:string_sub, "$(norm)", o]
      - predicates: cd:specifiedBy
        objects:
          mapping: specification-material-raws
          condition:
            - function: equal
              parameters:
                - [str1, $(id), s]
                - [str2, $(id), o]
      - predicates: cd:specifiedBy
        objects:
          mapping: datasheets
          condition:
            - function: equal
              parameters:
                - [str1, $(id), s]
                - [str2, $(id), o]

  specification-material-raws:
    sources: material-raws
    subjects:
      function: idlab-fn:concat
      parameters:
        - [idlab-fn:str, cd:]
        - parameter: idlab-fn:otherStr
          value:
            function: grel:string_sha1(grel:valueParameter = $(id))
    predicateobjects:
      - [rdfs:label, Chemical Composition]
      - [a, iof:RequirementSpecification~iri]
      - predicates: cd:requires
        objects:
          - mapping: chemical-composition
            condition:
              - function: equal
                parameters:
                  - [str1, $(id), s]
                  - [str2, $(id), o]

  material-forms:
    sources: material-forms
    subjects:
      function: idlab-fn:concat
      parameters:
        - [idlab-fn:str, cd:]
        - parameter: idlab-fn:otherStr
          value:
            function: grel:string_sha1
            parameters:
              - parameter: grel:valueParameter
                value:
                  function: idlab-fn:concat
                  parameters:
                    - [idlab-fn:str, $(norm)]
                    - [idlab-fn:otherStr, $(materialtype)]
    predicateobjects:
      - [a, cd:$(materialtype)~iri]
      - [rdfs:label, $(materialtype) ($(norm))]
      - predicates: rdfs:isDefinedBy
        objects:
          - mapping: norms
            condition:
              - function: grel:string_contains
                parameters:
                  - [grel:valueParameter, "$(norm)", s]
                  - [grel:string_sub, "$(norm)", o]
      - predicates: cd:specifiedBy
        objects:
          mapping: specification-material-raws
          condition:
            - function: equal
              parameters:
                - [str1, $(id), s]
                - [str2, $(id), o]

  chemical-composition:
    sources: chemical-compositions
    subjects:
      function: grel:array_join
      parameters:
        - [grel:p_array_a, cd:]
        - parameter: grel:p_array_a
          value:
            function: idlab-fn:random
            parameters: [] # This is needed atm, bug in the YARRRML parser
    predicateobjects:
      - [rdfs:label, $(limit_type) $(percentage)% of $(element)]
      - [a, cd:ChemicalElementFraction~iri]
      - [qudt:hasQuantityKind, qk:MassFraction~iri]
      - [qudt:unit, qudt:PERCENT~iri]
      - [cd:ofElement, cd:$(element)~iri]
      - predicates: cd:lessThan
        objects: $(percentage)
        condition:
          function: idlab-fn:equal
          parameters:
            - [grel:valueParameter, $(limit_type)]
            - [grel:valueParameter2, max]
      - predicates: cd:greaterThan
        objects: $(percentage)
        condition:
          function: idlab-fn:equal
          parameters:
            - [grel:valueParameter, $(limit_type)]
            - [grel:valueParameter2, min]

  norms:
    sources: norms
    subjects:
      fn: grel:array_join
      parameters:
        - [grel:p_array_a, cd:]
        - parameter: grel:p_array_a
          value:
            function: idlab-fn:random
            parameters: [] # This is needed atm, bug in the YARRRML parser
    predicateobjects:
      - [a, sto:Standard]
      - [rdfs:label, $(norm)]

  datasheets:
    sources: datasheets
    subjects: $(datasheet)
    predicateobjects:
      - [a, cd:CopperKeyDatasheet]
      - [rdfs:label, "Copper Key Datasheet"]
